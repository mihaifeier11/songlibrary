#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_songGUI.h"
#include "Service.h"
#include "Song.h"
#include <string>
#include <vector>
#include <qtableview.h>
#include <qtablewidget.h>
#include <qwidget.h>
#include <qstring.h>
#include <qboxlayout.h>
#include <qlabel.h>
#include <qformlayout.h>
#include <qobject.h>
#include <qpushbutton.h>
#include <qlineedit.h>

using namespace std;

class songGUI : public QMainWindow {
public:
	songGUI(Service & service) : service{ service } {
		init();
		connect();
		reloadTable(service.sorted());
	};
private:
	Service & service;
	QTableWidget * table;
	QLabel* artistC;
	QLabel* genC;
	QLineEdit* txtTitlu;
	QLineEdit* txtArtist;
	QLineEdit* txtGen;
	QPushButton* btnAdd;
	QPushButton* btnRemove;
	int ac = 0;
	int gc = 0;
	void init();
	void connect();
	void reloadTable(vector<Song> songs);

};
