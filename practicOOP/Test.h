#pragma once
class Test {
public:
	Test() = default;
	~Test() = default;
	void testAll();
	void testSong();
	void testRepository();
	void testService();
};

