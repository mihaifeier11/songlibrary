#pragma once
#include <string>
#include <vector>
using namespace std;
class Song {
private:
	int id;
	string titlu;
	string artist;
	string gen;

public:
	Song();
	Song(int id, string titlu, string artist, string gen);
	int getId();
	string getTitlu();
	string getArtist();
	string getGen();

	void operator=(Song song);

	~Song() = default;
};

