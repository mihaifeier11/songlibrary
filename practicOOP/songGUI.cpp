#include "songGUI.h"

void songGUI::init() {
	QHBoxLayout* hlayout = new QHBoxLayout;
	QWidget* widget = new QWidget;
	
	QVBoxLayout* tableLayout = new QVBoxLayout;
	QVBoxLayout* buttonLayout = new QVBoxLayout;
	hlayout->addLayout(tableLayout);
	hlayout->addLayout(buttonLayout);
	
	table = new QTableWidget(100, 4);
	
	QFormLayout* formlayout = new QFormLayout;


	tableLayout->addWidget(table);
	
	artistC = new QLabel(QString::number(ac));
	genC = new QLabel(QString::number(gc));

	formlayout->addRow(new QLabel("Artisti comuni: "), artistC);
	formlayout->addRow(new QLabel("Gen comun: "), genC);
	
	tableLayout->addLayout(formlayout);

	QFormLayout* btnform = new QFormLayout;
	txtTitlu = new QLineEdit;
	txtArtist = new QLineEdit;
	txtGen = new QLineEdit;

	btnform->addRow(new QLabel("Titlu: "), txtTitlu);
	btnform->addRow(new QLabel("Artist: "), txtArtist);
	btnform->addRow(new QLabel("Gen: "), txtGen);

	btnAdd = new QPushButton("Adauga");
	btnRemove = new QPushButton("Sterge");

	btnform->addRow(btnAdd);
	btnform->addRow(btnRemove);

	buttonLayout->addLayout(btnform);
	


	widget->setLayout(hlayout);
	widget->show();
}

void songGUI::connect() {
	QObject::connect(table, &QTableWidget::itemSelectionChanged, [&]() {
		QString s = table->currentItem()->text();
		
		int id = s.toInt();
		vector<int> same = service.acelasiAtribut(id);
		
		ac = same[0];
		gc = same[1];
		artistC->setText(QString::number(ac - 1));
		genC->setText(QString::number(gc - 1));
	});
	QObject::connect(btnAdd, &QPushButton::clicked, [&]() {
		int id = service.getMaxId() + 1;
		string titlu = txtTitlu->text().toStdString();
		string artist = txtArtist->text().toStdString();
		string gen = txtGen->text().toStdString();
		service.add(id, titlu, artist, gen);
		reloadTable(service.sorted());
	});
	QObject::connect(btnRemove, &QPushButton::clicked, [&]() {
		int id = table->currentItem()->text().toInt();
		service.remove(id);
		reloadTable(service.sorted());
		artistC->setText(QString::number(0));
		genC->setText(QString::number(0));
	});


}

void songGUI::reloadTable(vector<Song> songs) {
	table->clear();
	int i = 0;
	for (auto s : songs) {
		QTableWidgetItem* id = new QTableWidgetItem(QString::number(s.getId()));
		table->setItem(i, 0, id);
		QTableWidgetItem* titlu = new QTableWidgetItem(QString::fromStdString(s.getTitlu()));
		table->setItem(i, 1, titlu);
		QTableWidgetItem* artist = new QTableWidgetItem(QString::fromStdString(s.getArtist()));
		table->setItem(i, 2, artist);
		QTableWidgetItem* gen = new QTableWidgetItem(QString::fromStdString(s.getGen()));
		table->setItem(i, 3, gen);
		i++;
	}
}
