/********************************************************************************
** Form generated from reading UI file 'songGUI.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SONGGUI_H
#define UI_SONGGUI_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_songGUIClass
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *songGUIClass)
    {
        if (songGUIClass->objectName().isEmpty())
            songGUIClass->setObjectName(QStringLiteral("songGUIClass"));
        songGUIClass->resize(600, 400);
        menuBar = new QMenuBar(songGUIClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        songGUIClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(songGUIClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        songGUIClass->addToolBar(mainToolBar);
        centralWidget = new QWidget(songGUIClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        songGUIClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(songGUIClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        songGUIClass->setStatusBar(statusBar);

        retranslateUi(songGUIClass);

        QMetaObject::connectSlotsByName(songGUIClass);
    } // setupUi

    void retranslateUi(QMainWindow *songGUIClass)
    {
        songGUIClass->setWindowTitle(QApplication::translate("songGUIClass", "songGUI", nullptr));
    } // retranslateUi

};

namespace Ui {
    class songGUIClass: public Ui_songGUIClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SONGGUI_H
