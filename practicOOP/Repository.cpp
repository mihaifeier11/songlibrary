#include "Repository.h"
#include <fstream>
#include <iostream>
Repository::Repository(string file) {
	this->file = file;
}

vector<Song> Repository::getAll() {
	ifstream in(file);
	string line;
	string id;
	string titlu;
	string artist;
	string gen;

	char c;
	vector<Song> songs;
	while (!in.eof()) {
		in >> id;
		in >> titlu;
		in >> artist;
		in >> gen;
		Song song(stoi(id), titlu, artist, gen);
		songs.push_back(song);
	}



	in.close();
	
	return songs;
}

void Repository::write(vector<Song> songs) {
	ofstream out(file);
	for (int i = 0; i < songs.size() - 1; i++) {
		out << songs[i].getId() << endl;
		out << songs[i].getTitlu() << endl;
		out << songs[i].getArtist() << endl;
		out << songs[i].getGen() << endl;
	}

	out << songs[songs.size() - 1].getId() << endl;
	out << songs[songs.size() - 1].getTitlu() << endl;
	out << songs[songs.size() - 1].getArtist() << endl;
	out << songs[songs.size() - 1].getGen();


	out.close();
}

void Repository::add(Song song) {
	vector<Song> songs = getAll();
	songs.push_back(song);
	write(songs);

}

void Repository::remove(int id) {
	vector<Song> songs = getAll();
	vector<Song> temp;
	for (int i = 0; i < songs.size(); i++) {
		if (songs[i].getId() != id) {
			temp.push_back(songs[i]);
		}
	}
	write(temp);
}


