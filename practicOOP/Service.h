#pragma once
#include <string>
#include <vector>
#include "Repository.h"
#include "Song.h"
class Service {
private:
	string file;
	Repository& repo;
public:
	Service(Repository& repo) : repo{ repo } {};
	vector<Song> sorted();
	vector<int> acelasiAtribut(int id);
	void add(int id, string titlu, string artist, string gen);
	void remove(int id);
	~Service() = default;

	int getMaxId();
};

