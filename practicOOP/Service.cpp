#include "Service.h"
vector<Song> Service::sorted() {
	vector<Song> temp = repo.getAll();
	Song aux(0, "", "", "");
	for (int i = 0; i < temp.size(); i++) {
		for (int j = i; j < temp.size(); j++) {
			if (temp[i].getArtist() > temp[j].getArtist()) {
				aux = temp[i];
				temp[i] = temp[j];
				temp[j] = aux;
			}
		}
	}
	return temp;
}

vector<int> Service::acelasiAtribut(int id) {
	vector<Song> temp = repo.getAll();
	int autorC = 0;
	int genC = 0;
	vector<int> same;
	Song song(0, "", "", "");
	for (int i = 0; i < temp.size(); i++) {
		if (temp[i].getId() == id) {
			song = temp[i];
		}
	}
	for (int i = 0; i < temp.size(); i++) {
		if (temp[i].getArtist() == song.getArtist()) {
			autorC++;
		}
		if (temp[i].getGen() == song.getGen()) {
			genC++;
		}
	}

	same.push_back(autorC);
	same.push_back(genC);
	
	return same;
}

void Service::add(int id, string titlu, string artist, string gen){
	Song song(id, titlu, artist, gen);
	repo.add(song);
}

void Service::remove(int id) {
	repo.remove(id);
}

int Service::getMaxId() {
	vector<Song> temp = repo.getAll();
	int max = 0;
	for (int i = 0; i < temp.size(); i++) {
		if (temp[i].getId() > max) {
			max = temp[i].getId();
		}
	}
	return max;
}

