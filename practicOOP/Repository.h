#pragma once
#include <string>
#include <vector>
#include "Song.h"
using namespace std;

class Repository {
private:
	string file;
public:
	Repository(string file);
	vector<Song> getAll();
	void add(Song song);
	void remove(int id);
	void write(vector<Song> songs);
	~Repository() = default;
};

