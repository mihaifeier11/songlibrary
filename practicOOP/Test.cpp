#include "Test.h"
#include "Song.h"
#include "Repository.h"
#include <assert.h>
#include "Service.h"

void Test::testAll() {
	testSong();
	testRepository();
	testService();
}

void Test::testSong() {
	Song song(0, "titlu", "artist", "gen");
	assert(song.getId() == 0);
	assert(song.getTitlu() == "titlu");
	assert(song.getArtist() == "artist");
	assert(song.getGen() == "gen");
}

void Test::testRepository() {
	Song song(0, "titlu", "artist", "gen");
	Song song2(1, "titlu", "artist", "gen");
	Repository repo("test.txt");
	vector<Song> songs;
	songs.push_back(song);
	songs.push_back(song2);
	
	repo.write(songs);
	songs = repo.getAll();
	assert(songs.size() == 2);
	
	repo.remove(1);
	songs = repo.getAll();
	assert(songs.size() == 1);
	
	repo.add(song);
	songs = repo.getAll();
	assert(songs.size() == 2);

}

void Test::testService() {
	Song song2(1, "titlu", "artist", "gen");
	Repository repo("test.txt");
	Service service{ repo };
	service.add(1, "titlu", "artist", "gen");
	vector<Song> songs = service.sorted();
	assert(songs.size() == 3);
	vector<int> same = service.acelasiAtribut(1);
	assert(same[0] == 3);
	assert(same[1] == 3);
	assert(service.getMaxId() == 1);
	service.remove(1);
	songs = service.sorted();
	assert(songs.size() == 2);
	assert(service.getMaxId() == 0);
}
