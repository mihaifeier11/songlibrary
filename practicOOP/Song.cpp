#include "Song.h"

Song::Song(int id, string titlu, string artist, string gen) {
	this->id = id;
	this->titlu = titlu;
	this->artist = artist;
	this->gen = gen;
}

int Song::getId() {
	return this->id;
}

string Song::getTitlu() {
	return this->titlu;
}

string Song::getArtist() {
	return this->artist;
}

string Song::getGen() {
	return this->gen;
}

void Song::operator=(Song song) {
	this->id = song.getId();
	this->titlu = song.getTitlu();
	this->artist = song.getArtist();
	this->gen = song.getGen();
}
