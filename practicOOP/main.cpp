#include "songGUI.h"
#include <QtWidgets/QApplication>
#include "Test.h"
#include "Repository.h"
#include "Service.h"

int main(int argc, char *argv[]) {
	QApplication a(argc, argv);
	Test test;
	test.testAll();
	
	Repository repo("song.txt");
	Service service{ repo };
	songGUI w{ service };
	//w.show();
	return a.exec();
}
